#![no_std]
#![cfg_attr(feature = "const-fn", feature(const_fn))]

use heapless::{
    ArrayLength,
    Vec,
};

use core::{
    iter::{
        self,
        ExactSizeIterator,
        IntoIterator,
        Iterator,
    },
    mem,
    slice,
};

#[derive(Ord, PartialOrd, Eq, PartialEq, Clone, Copy, Debug)]
struct Generation {
    _0: usize,
}

#[derive(Ord, PartialOrd, Eq, PartialEq, Clone, Copy, Debug)]
pub struct Index {
    _0: usize,
    gen: Generation,
}

#[derive(Eq, PartialEq, Clone, Copy, Debug)]
pub struct Entry<T>(EntryImpl<T>);

#[derive(Eq, PartialEq, Clone, Copy, Debug)]
enum EntryImpl<T> {
    Occupied(T, Generation),
    Next(usize),
    Empty,
}

impl<T> Entry<T> {
    fn value(self) -> Option<T> {
        match self.0 {
            EntryImpl::Occupied(value, _) => Some(value),
            _ => None,
        }
    }
    fn value_ref(&self) -> Option<&T> {
        match self.0 {
            EntryImpl::Occupied(ref value, _) => Some(value),
            _ => None,
        }
    }
    fn value_mut(&mut self) -> Option<&mut T> {
        match self.0 {
            EntryImpl::Occupied(ref mut value, _) => Some(value),
            _ => None,
        }
    }
    fn gen(&self) -> Option<Generation> {
        match self.0 {
            EntryImpl::Occupied(_, gen) => Some(gen),
            _ => None,
        }
    }
    fn next(self) -> Option<usize> {
        match self.0 {
            EntryImpl::Next(free) => Some(free),
            _ => None,
        }
    }
}

#[derive(Eq, PartialEq, Debug)]
pub struct Arena<T, N: ArrayLength<Entry<T>>> {
    data: Vec<Entry<T>, N>,
    gen: Generation,
    free: Option<usize>,
    len: usize,
}

impl<T, N> Default for Arena<T, N>
where
    N: ArrayLength<Entry<T>>,
{
    fn default() -> Self {
        Arena::new()
    }
}

impl<T, N> Arena<T, N>
where
    N: ArrayLength<Entry<T>>,
{
    #[cfg(not(feature = "const-fn"))]
    pub fn new() -> Arena<T, N> {
        Arena {
            data: Vec::new(),
            gen: Generation { _0: 0 },
            free: None,
            len: 0,
        }
    }

    #[cfg(feature = "const-fn")]
    pub const fn new() -> Arena<T, N> {
        Arena {
            data: Vec::new(),
            gen: Generation { _0: 0 },
            free: None,
            len: 0,
        }
    }

    pub fn insert(&mut self, value: T) -> Result<Index, T> {
        let index = if let Some(free) = self.free.take() {
            let old = mem::replace(
                &mut self.data[free],
                Entry(EntryImpl::Occupied(value, self.gen)),
            );
            assert!(old.gen().is_none());
            self.free = old.next();
            free
        } else {
            self.data
                .push(Entry(EntryImpl::Occupied(value, self.gen)))
                .map_err(|e| e.value().unwrap())?;
            self.data.len() - 1
        };
        self.len += 1;
        Ok(Index {
            _0: index,
            gen: self.gen,
        })
    }

    pub fn remove(&mut self, index: Index) -> Option<T> {
        if let Some(entry) = self.data.get_mut(index._0) {
            if entry.gen() == Some(index.gen) {
                let value = mem::replace(
                    entry,
                    self.free
                        .take()
                        .map(|free| Entry(EntryImpl::Next(free)))
                        .unwrap_or(Entry(EntryImpl::Empty)),
                )
                .value()
                .unwrap();
                self.free = Some(index._0);
                self.gen._0 = self.gen._0.wrapping_add(1);
                self.len -= 1;
                return Some(value);
            }
        }

        None
    }

    pub fn get(&self, index: Index) -> Option<&T> {
        if let Some(entry) = self.data.get(index._0) {
            if entry.gen() == Some(index.gen) {
                return entry.value_ref();
            }
        }

        None
    }

    pub fn get_mut(&mut self, index: Index) -> Option<&mut T> {
        if let Some(entry) = self.data.get_mut(index._0) {
            if entry.gen() == Some(index.gen) {
                return entry.value_mut();
            }
        }

        None
    }

    pub fn contains(&self, index: Index) -> bool {
        self.data
            .get(index._0)
            .and_then(|entry| entry.gen())
            .map(|gen| gen == index.gen)
            .unwrap_or(false)
    }

    pub fn clear(&mut self) {
        self.data.clear();
        self.free = None;
        self.len = 0;
    }

    pub fn len(&self) -> usize {
        self.len
    }

    pub fn iter(&self) -> Iter<T> {
        Iter {
            len: self.len(),
            it: self.data.iter().enumerate(),
        }
    }

    pub fn iter_mut(&mut self) -> IterMut<T> {
        IterMut {
            len: self.len(),
            it: self.data.iter_mut().enumerate(),
        }
    }
}

pub struct Iter<'a, T> {
    len: usize,
    it: iter::Enumerate<slice::Iter<'a, Entry<T>>>,
}

impl<'a, T> Iterator for Iter<'a, T> {
    type Item = (Index, &'a T);
    fn next(&mut self) -> Option<Self::Item> {
        self.it.next().and_then(|(i, e)| {
            Some((
                Index {
                    _0: i,
                    gen: e.gen()?,
                },
                e.value_ref()?,
            ))
        })
    }
}

impl<'a, T> ExactSizeIterator for Iter<'a, T> {
    fn len(&self) -> usize {
        self.len
    }
}

pub struct IterMut<'a, T> {
    len: usize,
    it: iter::Enumerate<slice::IterMut<'a, Entry<T>>>,
}

impl<'a, T> Iterator for IterMut<'a, T> {
    type Item = (Index, &'a mut T);
    fn next(&mut self) -> Option<Self::Item> {
        self.it.next().and_then(|(i, e)| {
            Some((
                Index {
                    _0: i,
                    gen: e.gen()?,
                },
                e.value_mut()?,
            ))
        })
    }
}

impl<'a, T> ExactSizeIterator for IterMut<'a, T> {
    fn len(&self) -> usize {
        self.len
    }
}

impl<'a, T, N> IntoIterator for &'a mut Arena<T, N>
where
    N: ArrayLength<Entry<T>>,
{
    type IntoIter = IterMut<'a, T>;
    type Item = (Index, &'a mut T);
    fn into_iter(self) -> Self::IntoIter {
        self.iter_mut()
    }
}

impl<'a, T, N> IntoIterator for &'a Arena<T, N>
where
    N: ArrayLength<Entry<T>>,
{
    type IntoIter = Iter<'a, T>;
    type Item = (Index, &'a T);
    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

#[cfg(test)]
mod tests {

    use super::*;

    use typenum::U4;

    #[test]
    fn arena() {
        let mut arena: Arena<u8, U4> = Arena::default();
        let _zero = arena.insert(0).unwrap();
        let one = arena.insert(1).unwrap();
        let _two = arena.insert(2).unwrap();
        let _three = arena.insert(3).unwrap();
        assert!(arena.len() == 4);
        assert!(arena.insert(4) == Err(4));
        assert!(arena.remove(one).unwrap() == 1);
        assert!(arena.len() == 3);
        let four = arena.insert(4).unwrap();
        assert!(arena.len() == 4);
        assert!(arena.get(one).is_none());
        assert!(*arena.get(four).unwrap() == 4);
        assert!(one._0 == four._0);
    }
}
